/**
 * @param {...*} args
 */
jQuery.prototype.dialog = function(args) {};

/**
 * @param {string} name
 * @param {!*} base
 * @param {*=} prototype
 */
jQuery.widget = function(name, base, prototype) {};
